/*
    NDBlink.cpp - Library for blinking leds without delays.
    Created by bretagne.bastian@gmail.com in 2021
*/

#include <Arduino.h>
#include "NDBlink.h"

NDBlink::NDBlink(int pin, long on, long off){
	ledPin = pin;
	pinMode(ledPin, OUTPUT);

	onTime = on;
	offTime = off;

	ledState = LOW;
	previousMillis = 0;
};

void NDBlink::Update(){
    unsigned long currentMillis = millis();
    if((ledState == HIGH) && (currentMillis - previousMillis >= onTime)){
        ledState = LOW;
        previousMillis = currentMillis;
        digitalWrite(ledPin, ledState);
    } else if ((ledState == LOW) && (currentMillis - previousMillis >= offTime)){
        ledState = HIGH;
        previousMillis = currentMillis;
        digitalWrite(ledPin, ledState);
    }
};