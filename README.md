# No Delay - Blink Led

## Example

### Basic Example, blink every 1sec

```cpp
#include "Arduino.h"
#include "NDBlink.h"

const uint8_t led_pin = LED_BUILTIN;
unsigned long on_time = 1000;
unsigned long off_time = 1000;
NDBlink blinker(led_pin, on_time, off_time);

void setup(){
    Serial.begin(9600);
}

void loop(){
    blinker.Update();
}
```

### Use as dimmer, how to dim led

```cpp
#include "Arduino.h"
#include "NDBlink.h"

const uint8_t led_pin = LED_BUILTIN;
unsigned long on_time = 2;
unsigned long off_time = 10;
NDBlink blinker(led_pin, on_time, off_time);

void setup(){
    Serial.begin(9600);
}

void loop(){
    blinker.Update();
}
```
