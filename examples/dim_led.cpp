#include "Arduino.h"
#include "NDBlink.h"

const uint8_t led_pin = LED_BUILTIN;
unsigned long on_time = 2; // Adjuste those variables to dim the led
unsigned long off_time = 10;
NDBlink blinker(led_pin, on_time, off_time);

void setup(){
    Serial.begin(9600);
}

void loop(){
    blinker.Update();
}