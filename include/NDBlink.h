/*
    NDBlink.cpp - Library for blinking leds without delays.
    Created by bretagne.bastian@gmail.com in 2021
*/

#ifndef NDBlink_H
#define NDBlink_H

#include <Arduino.h>

class NDBlink{
    private:
        int ledPin;
        unsigned long onTime;
        unsigned long offTime;
        int ledState;
        unsigned long previousMillis;

    public:
        NDBlink(int pin, long on, long off);
        void Update();
};

#endif